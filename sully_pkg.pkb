create or replace package body sully_pkg is

    procedure do_nothing
    is
        v_dummy varchar2(200);
    begin
        select dummy
        into v_dummy
        from dual;

    end do_nothing;

    -- more nothing to do
    procedure do_nothing_again
    is
        v_dummy varchar2(200);
    begin
        select dummy
        into v_dummy
        from dual;

    end do_nothing;

end sully_pkg;